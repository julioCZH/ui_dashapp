import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginSubscription: Subscription;
  constructor(private fb: FormBuilder, private loginService: AuthService) { 
    this.loginForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.pattern(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)]),
      password: new FormControl('',Validators.required),
    });
  }

  ngOnInit(): void {
  }

  onSubmit(){
    const data = { 
      username: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    }
    this.loginSubscription = this.loginService.doLogin(data).subscribe(res => {
      console.log('Response:',res)
    })
  }
  
}
