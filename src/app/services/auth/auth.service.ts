import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loginEndpoint = 'http://localhost:8000/api/login/';
  loginSubscription: Subscription;

  constructor(private http: HttpClient) {

  }

   doLogin(data){
     return this.http.post(this.loginEndpoint, data);
   }
}
